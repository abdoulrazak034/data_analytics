import os
import numpy as np


def test_data_files_exist():
    files = [
        "data/train_features.csv",
        "data/test_features.csv",
        "data/train_labels.csv",
        "data/test_labels.csv",
    ]
    for file in files:
        assert os.path.isfile(file)


def test_data_shape():
    X_train = np.genfromtxt("data/train_features.csv", delimiter=",")
    y_train = np.genfromtxt("data/train_labels.csv", delimiter=",")
    X_test = np.genfromtxt("data/test_features.csv", delimiter=",")
    y_test = np.genfromtxt("data/test_labels.csv", delimiter=",")

    assert X_train.shape[0] == y_train.shape[0]
    assert X_test.shape[0] == y_test.shape[0]
    assert X_train.shape[1] == X_test.shape[1]
