import os
from train import clf, acc


def test_model_training():
    assert clf is not None
    assert acc > 0


def test_plot_exists():
    assert os.path.isfile("plot.png")


def test_metrics_file():
    with open("metrics.txt", "r") as f:
        lines = f.readlines()
        assert len(lines) > 0
        assert "Accuracy" in lines[0]
