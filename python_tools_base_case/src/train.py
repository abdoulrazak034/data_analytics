"""
This module trains a RandomForestClassifier and saves the model performance
metrics and plots.
"""

import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import ConfusionMatrixDisplay
import matplotlib.pyplot as plt

# Read in data
X_train = np.genfromtxt("data/train_features.csv")
y_train = np.genfromtxt("data/train_labels.csv")
X_test = np.genfromtxt("data/test_features.csv")
y_test = np.genfromtxt("data/test_labels.csv")

# Fit a model
DEPTH = 5
clf = RandomForestClassifier(max_depth=DEPTH, n_jobs=-1, verbose=1)
clf.fit(X_train, y_train)

acc = clf.score(X_test, y_test)
print(acc)

metrics = f"""
Accuracy: {acc:10.4f}

![Confusion Matrix](plot.png)
"""
with open("metrics.txt", "w", encoding="utf-8") as outfile:
    outfile.write(metrics)

# Plot it
disp = ConfusionMatrixDisplay.from_estimator(
    clf, X_test, y_test, normalize="true", cmap=plt.get_cmap("Blues")
)
disp.figure_.savefig("plot.png")
