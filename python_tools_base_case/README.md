
# My Machine Learning Tools Portfolio

Ce dépôt contient des exemples de tests et de configurations pour différents outils Python, notamment Black, Flake8, Mypy, Pylint et Pytest, dans le contexte d'un projet de machine learning.

## Installation

1. Créez un environnement virtuel et activez-le :
   ```bash
   python -m venv venv
   source venv/bin/activate  # Sur Windows, utilisez `venv\Scripts\activate`
   ```

2. Installez les dépendances :
   ```bash
   pip install -r requirements.txt
   ```

3. Générez les données :
   ```bash
   python src/get_data.py
   ```

4. Entraînez le modèle :
   ```bash
   python src/train.py
   ```

## Utilisation des outils

### Black

Pour formater le code :
```bash
black src/ tests/
```

### Flake8

Pour vérifier les erreurs de style :
```bash
flake8 src/ tests/
```

### Mypy

Pour vérifier les types :
```bash
mypy src/
```

### Pylint

Pour analyser le code :
```bash
pylint src/
```

### Pytest

Pour exécuter les tests :
```bash
pytest
```

## Exemple de commande pour chaque outil

- Black : `black .`
- Flake8 : `flake8`
- Mypy : `mypy src/`
- Pylint : `pylint src/`
- Pytest : `pytest`
