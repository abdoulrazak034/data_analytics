import serial
from bokeh.plotting import figure, curdoc
from bokeh.models import ColumnDataSource
from bokeh.layouts import column, row
import datetime

# configuration du port série pour se connecter à la carte Arduino
ser = serial.Serial('COM6', 9600, timeout=1)

# création des sources de données pour la température et l'humidité
source_temp = ColumnDataSource({'x': [], 'y': []})
source_hum = ColumnDataSource({'x': [], 'y': []})

# création du graphique pour la température
fig_temp = figure(title='Température', x_axis_label='Temps', y_axis_label='Température (°C)')
fig_temp.line('x', 'y', source=source_temp, line_width=2, color='blue')

# création du graphique pour l'humidité
fig_hum = figure(title='Humidité', x_axis_label='Temps', y_axis_label='Humidité (%)')
fig_hum.line('x', 'y', source=source_hum, line_width=2, color='green')

# mise en page des graphiques
plots = row(fig_temp, fig_hum)

# fonction de mise à jour des données
def update():
    # lecture des données depuis le port série
    line = ser.readline().decode('utf-8').strip()
    data = line.split(',')
    print(data)
    if len(data) == 2:
        temp = float(data[0])
        hum = float(data[1])
        time = datetime.datetime.now()

        # mise à jour des sources de données
        source_temp.stream({'x': [time], 'y': [temp]})
        source_hum.stream({'x': [time], 'y': [hum]})


        # imprime les valeurs courantes sur le graphique
        fig_temp.title.text = f'Température (actuelle: {temp:.1f} °C)'
        fig_hum.title.text = f'Humidité (actuelle: {hum:.1f} %)'
# ajout de la fonction de mise à jour à la boucle de document bokeh
curdoc().add_periodic_callback(update, 1000)

# ajout des graphiques à la page web
curdoc().title = 'Capteurs de température et d\'humidité'
curdoc().add_root(plots)
