# Data Analytics

## Overview

This repository contains a project that integrates an Arduino with a DHT11 sensor to visualize temperature and humidity data in real-time using Bokeh. The `bokeh_arduino_dht11.py` script starts a Bokeh server that displays the sensor data in a web browser.

## Getting Started

Follow these steps to set up and run the script:

### Prerequisites

Ensure you have the following dependencies installed:

- Python
- Bokeh
- Arduino IDE
- Necessary libraries for Arduino and DHT11 sensor communication

### Steps

1. **Open a Terminal**
   - Navigate to the directory containing the `bokeh_arduino_dht11.py` script.

2. **Install Dependencies**
   - Install Bokeh and other required libraries using pip:
     ```sh
     pip install bokeh
     ```
   - Install any additional libraries necessary for communication with the Arduino and DHT11 sensor.

3. **Connect Arduino**
   - Connect your Arduino to your computer.
   - Ensure it is properly configured and the required code is uploaded.

4. **Run the Script**
   - Start the Bokeh server and display the app in your web browser:
     ```sh
     bokeh serve --show bokeh_arduino_dht11.py
     ```
   - This command will launch the Bokeh server and open the app in your default web browser. You should see real-time data from the DHT11 sensor.

## Keywords

- Python
- Bokeh
- Sensors
- Temperature
- Humidity
- Visualization
- Data Science
- IoT
- Raspberry Pi
- Programming
- Computing
- Machine Learning
- Data Processing
- Data Analysis
- Data Visualization
- Data Science Projects
- IoT Projects
- Technology

## Repository Structure

- `DHT11.ino`: Arduino code for reading data from the DHT11 sensor.
- `README.md`: This README file.
- `bokeh_arduino_dht11.py`: Python script to start the Bokeh server and visualize the data.

## Editing This README

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this `README.md` and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!
